#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2012 Ola Sikström
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function

import os
import pickle

import Tkinter as T
from tkSimpleDialog import Dialog


SETTINGS_FILE = '.temperature_settings'
TEMPLATES_DIR = 'machine_templates'

DEFAULT_SETTINGS = {
    'T0_TEMP': 220,
    'T1_TEMP': 220,
    'HBP_TEMP': 100,
}

LABELS = {
    'HBP_TEMP': 'HBP Temp.',
    'T0_TEMP': 'Toolhead 0 Temp.',
    'T1_TEMP': 'Toolhead 1 Temp.',
}

def get_label(code):
    if code in LABELS:
        return LABELS[code]
    return code


def load_settings():
    """
    Load pickled settings, or use defaults
    """
    settings = DEFAULT_SETTINGS.copy()

    try:
        with file(SETTINGS_FILE, 'r') as settingfile:
            settings.update( pickle.load(settingfile) )
    except IOError:
        pass

    return settings
    

def save_settings(settings):
    """
    pickle settings to file
    """
    with file(SETTINGS_FILE, 'w') as settingfile:
        pickle.dump(settings, settingfile)


def process_templates(settings):
    """
    Update g-code files in 'machines'
    based on files in 'machine_templates'
    """
    for path, dirs, files in os.walk(TEMPLATES_DIR):
        for f in files:
            if f.endswith('.gcode'):
                template_path = os.path.join(path, f)
                gcode_path = template_path.replace(
                        TEMPLATES_DIR, 'machines')

                # process file
                with file(template_path) as f:
                    contents = f.read()

                print('writing:', gcode_path)
                with file(gcode_path, 'w') as f:
                    f.write( contents.format( **settings ) )


class TemperatureDialog(Dialog):
    """
    Simple Tkinter dialog, with fields generated from the settings-dict
    """
    def __init__(self):
        root = T.Tk()
        root.withdraw()
        Dialog.__init__(self, root, 'Temperature Utility')


    def body(self, master):
        settings = load_settings()

        self._values = {}

        grid_options = {
            'sticky':  T.W,
            'padx':    3,
            'pady':    3
        }

        # build labels and fields from settings-dict
        for i,(k,v) in enumerate(settings.items()):
            self._values[k] = var = T.IntVar()
            var.set(v)

            label = T.Label(master, text=get_label(k))
            entry = T.Entry(master, textvariable=var)

            label.grid(row=i, column=0, **grid_options)
            entry.grid(row=i, column=1, **grid_options)


    def apply(self):
        settings = dict((k, v.get()) for k,v in self._values.items())
        save_settings(settings)
        process_templates(settings)


def error(msg):
    """
    Show error-dialog and exit
    """
    import sys
    import tkMessageBox

    root=T.Tk()
    root.withdraw()
    tkMessageBox.showerror('Error', msg)

    sys.exit(1)


if __name__ == '__main__':
    if not os.path.exists('replicatorg.exe'):
        error('Script needs to be placed inside replicatorg-directory.')

    if not os.path.isdir(TEMPLATES_DIR):
        error('No templates dir found.')

    dialog = TemperatureDialog()





