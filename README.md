ReplicatorG Temperature Utility
===============================

About
-----

This is a simple utility to update temperature settings
for ReplicatorG when using skeinforge as GCode generator.

Usage
-----

Simply run set_temperatures.py, and you will be presented with
a dialog to enter temperatures.

Press OK to write the start.gcode files.

WARNING: Your start.gcode-files will be overwritten by those in
machine_templates/. Make sure to save any changes you want to keep.


Be aware of the following:
* All template files will be processed each time.
* You will need to slice your model again after updating temperatures.
* Double check your generated GCode for the temperature changes, since the start.gcode you are using might not be included in the templates.


Installation
------------

Place the following in the root directory of ReplicatorG:

* set_temperatures.py
* machine_templates

### For windows users:
You might want to rename set_temperatures.py to set_temperatures.pyw, if you find the command window annoying...


Requirements
------------

* ReplicatorG
* Python 2.6 or 2.7

This is also required to run ReplicatorG with skeinforge, so you probably already have it installed.

Authors
------

By: Ola Sikstr�m - ola.sikstrom@gmail.com
For: Creative Tools Sweden AB - http://www.creativetools.se/

License
-------
Apache License 2.0

